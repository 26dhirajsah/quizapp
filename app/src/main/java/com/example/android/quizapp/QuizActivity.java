package com.example.android.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;

public class QuizActivity extends AppCompatActivity {

    private Button submitButton;
    private int score;
    private RadioGroup radioGroupOne;
    private RadioGroup radioGroupTwo;
    private RadioGroup radioGroupThree;
    private EditText image_answer;
    private CheckBox kotlin;
    private CheckBox java;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        submitButton = findViewById(R.id.submit_button);
        image_answer=findViewById(R.id.image_answer);
        kotlin=findViewById(R.id.kotlin);
        java=findViewById(R.id.java);
        radioGroupOne = findViewById(R.id.radio_group_one);
        radioGroupTwo = findViewById(R.id.radio_group_two);
        radioGroupThree = findViewById(R.id.radio_group_three);


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                score=0;
                String answer = image_answer.getText().toString();
                if (answer.equalsIgnoreCase("koshi")){
                    score += 1;
                }
                if (java.isChecked() && kotlin.isChecked()){
                    score += 1;
                }
                if (radioGroupOne.getCheckedRadioButtonId()==R.id.wuhan){
                    score += 1;
                }
                if (radioGroupTwo.getCheckedRadioButtonId()==R.id.all){
                    score += 1;
                }
                if (radioGroupThree.getCheckedRadioButtonId()==R.id.italy){
                    score += 1;
                }

                Intent intent = new Intent(QuizActivity.this,ScoreActivity.class);
                intent.putExtra("SCORE",score+"/5");
                startActivity(intent);
            }
        });

        /*radioGroupTwo.clearCheck();
        radioGroupTwo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId==R.id.all)
                    score += 1;
            }
        });

        radioGroupThree.clearCheck();
        radioGroupThree.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId==R.id.italy)
                    score += 1 ;
            }
        });*/
    }
}
